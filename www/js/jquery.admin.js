$(document).ready(function() {
  $('.closeForm').click(function() {
    $('.formAdmin').fadeOut('slow');
    $('.formAdminSec').fadeOut('slow');
  });
	$('.closeFormSec').click(function() {
    $('.formAdminSec').fadeOut('slow');
  });
  $('button.help').click(function() {
      var posHelp = parseFloat($('#helpContent').css('left'));
      if (posHelp == -630) {
          $('#helpContent').animate({left: '0px'}, 'slow');
      } else if (posHelp == 0){
         $('#helpContent').animate({left: '-630px'}, 'slow'); 
      }   
  });
  $('img.closeHelp').click(function() {
      $('#helpContent').animate({left: '-630px'}, 'slow');
  });
	$('input.empty').click(function() {
      $(this).val('');
  });
});
$(document).ready(function() {
        function suma () {
            // select1
            var pt1 = parseInt($('table td.p1').html().substring(2));
            var it1 = parseInt($('select.select1').val());
            var tt1 = it1 * pt1;
            // select2
            if ($('select').hasClass("select2") == true) {
		var pt2 = parseInt($('table td.p2').html().substring(2));
		var it2 = parseInt($('select.select2').val());
		var tt2 = it2 * pt2;
            } else {
                tt2 = 0;
            } 
            // select3
            if ($('select').hasClass("select3") == true) {
		var pt3 = parseInt($('table td.p3').html().substring(2));
		var it3 = parseInt($('select.select3').val());
		var tt3 = it3 * pt3;
            }
            // select4
            if ($('select').hasClass("select4") == true) {
		var pt4 = parseInt($('table td.p4').html().substring(2));
		var it4 = parseInt($('select.select4').val());
		var tt4 = it4 * pt4;
            } else {
                tt4 = 0;
            }
            // select5
            if ($('select').hasClass("select5") == true) {
		var pt5 = parseInt($('table td.p5').html().substring(2));
		var it5 = parseInt($('select.select5').val());
		var tt5 = it5 * pt5;
            }
            
            var totalt = tt1 + tt2 + tt3 + tt4 + tt5;

            $('table td.a6').html('&euro; ' + totalt + ',00');
        };
	
        // DEFAULT
        var total = 0;
        if ($('select').hasClass("select1") == true) {
                var p1 = parseInt($('table td.p1').html().substring(2));
                var i1 = parseInt($('select.select1').val());
                var t1 = i1 * p1;
                total = total + t1;
                $('table td.a1').html('&euro; ' + total + ',00');
        }
    
	if ($('select').hasClass("select2") == true) {
		var p2 = parseInt($('table td.p2').html().substring(2));
		var i2 = parseInt($('select.select2').val());
		var t2 = i2 * p2;
                total = total + t2;
		$('table td.a2').html('&euro; ' + t2 + ',00');
	}
	
	if ($('select').hasClass("select3") == true) {
		var p3 = parseInt($('table td.p3').html().substring(2));
		var i3 = parseInt($('select.select3').val());
		var t3 = i3 * p3;
                total = total + t3;
		$('table td.a3').html('&euro; ' + t3 + ',00');
	}

	if ($('select').hasClass("select4") == true) {
		var p4 = parseInt($('table td.p4').html().substring(2));
		var i4 = parseInt($('select.select4').val());
		var t4 = i4 * p4;
                total = total + t4;
		$('table td.a4').html('&euro; ' + t4 + ',00');
	}

	if ($('select').hasClass("select5") == true) {
		var p5 = parseInt($('table td.p5').html().substring(2));
		var i5 = parseInt($('select.select5').val());
		var t5 = i5 * p5;
                total = total + t5;
		$('table td.a5').html('&euro; ' + t5 + ',00');
	}
        $('table td.a6').html('&euro; ' + total + ',00');
                       
        // CHANGE
        $('select.select1').change(function() {
            p1 = parseInt($('table td.p1').html().substring(2));
            i1 = parseInt($('select.select1').val());
            t1 = i1 * p1;
            $('table td.a1').html('&euro; ' + t1 + ',00');
            //total
            suma();        
        });
        
        if ($('select').hasClass("select2") == true) {
            $('select.select2').change(function(){
                p2 = parseInt($('table td.p2').html().substring(2));
                i2 = parseInt($('select.select2').val());
                t2 = i2 * p2;
                $('table td.a2').html('&euro; ' + t2 + ',00');
                // total
                suma();
            }); 
        }        
        
        if ($('select').hasClass("select3") == true) {
            $('select.select3').change(function(){
                p3 = parseInt($('table td.p3').html().substring(2));
                i3 = parseInt($('select.select3').val());
                t3 = i3 * p3;
                $('table td.a3').html('&euro; ' + t3 + ',00');
                // total
                suma();
            }); 
        }
        
        if ($('select').hasClass("select4") == true) {
            $('select.select4').change(function(){
                p4 = parseInt($('table td.p4').html().substring(2));
                i4 = parseInt($('select.select4').val());
                t4 = i4 * p4;
                $('table td.a4').html('&euro; ' + t4 + ',00');
                //total
                suma(); 
            }); 
        }
        
        if ($('select').hasClass("select5") == true) {
            $('select.select5').change(function(){
                p5 = parseInt($('table td.p5').html().substring(2));
                i5 = parseInt($('select.select5').val());
                t5 = i5 * p5;
                $('table td.a5').html('&euro; ' + t5 + ',00');
                //total
                suma(); 
            }); 
        }
        
});
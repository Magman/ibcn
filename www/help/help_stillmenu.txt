<div id="helpContent">
<img class="closeHelp" src="../../design/admin/delete.gif" alt="zavřít" title="zavřít">
<h3>Stálé menu</h3>
<p>
Sekce <strong>Stálé menu</strong> slouží k zobrazení a správě stálého menu restaurace (stálý jídelní lístek, nápojový lístek). Stálé menu lze spravovat ve dvou režimech:
<ol>
<li>Strukturované stálé menu</li>
<li>Polední menu v externím PDF souboru</li>
</ol>
</p>
<p>
Režim <strong>Strukturované stálé menu</strong> slouží k sestavení menu restaurace navázané na <strong>skupinu</strong> (předkrmy, polévky, hlavní jídla, dezerty apod). Umožňuje:
<ol>
<li><strong>Vložit skupinu</strong> (vloží skupinu s prázdnou položkou menu)</li>
<li><strong>Přidávat položky menu</strong> k jednotlivým skupinám, <strong>editovat</strong> je a <strong>mazat</strong>.</li>
<li><strong>Měnit pořadí</strong> položek menu (provádí se pouhým přetažením na zvolenou pozici)</li>
<li><strong>Mazat skupinu</strong> včetně všech položek menu</li>
<li><strong>Měnit pořadí</strong> položek skupiny (provádí se pouhým přetažením na zvolenou pozici)</li>
</ol>
Dále umožňuje vybrat v kterých jazycích se bude menu zobrazovat ve frontendu nezávisle na aktivních jazycích frontendu:
<ol>
<li>ve všech aktivních jazycích (odpovídající aktivní jazyky)</li>
<li>v Češtině a Angličtině (ostatní jazyky kromě Češtiny)</li>
<li>pouze v Angličtině (všechny jazyky)</li>
</ol>
</p>
<p>
Režim <strong>Stálé menu v externím PDF souboru</strong> umožňuje načítat a mazat externí PDF soubor. PDF bude zobrazováno ve vloženém rámci (iframe).
</p>
</div>

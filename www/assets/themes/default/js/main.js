$(document).ready(function(){
	
	$(document).on('submit', '#ajax-create-package', function(e){
		e.preventDefault();
		$.nette.ajax({
        validate: false
    }, this, e);
		
		$('#createPackage').modal('show');
	});
	
	// Listen for click on toggle checkbox
	$(document).on('click', '#select-all', function (event) {
		var checked = this.checked;
		if($('.selected').length > 1000){
			alert('Max. počet transakcí ve výběru je 1000.');
			this.checked = false;
		}else{
			// Iterate each checkbox
			$('.selected').each(function () {
				this.checked = checked;
			});
		}
		var button = $('[data-select-text]');
		if(this.checked){
			button.text(button.data('select-text'));
		}else{
			button.text(button.data('unselect-text'));
		}
	});
	
	$(document).on('click', '.selected', function(){
			if($('.selected:checked').length > 1000){
				alert('Max. počet transakcí ve výběru je 1000.');
				this.checked = false;
			}
			
			var button = $('[data-select-text]');
			if($('.selected:checked').length >= 1){
				button.text(button.data('select-text'));
			}else{
				button.text(button.data('unselect-text'));
			}
	});
	
	$('.select2').select2({theme: "bootstrap"});
	$(document).on('change', '#frm-filter-task', function () {
		var url = $("#snippet--update").data('url') + '&type=templates&filterId=' + $(this).val();
		$.nette.ajax({
			url: url,
			complete: function () {
				$('.select2').select2({theme: "bootstrap"});
			}
		});
	});
	$(document).on('change', '#frm-filter-client, #frm-importForm-clients', function () {
		var url = $("#snippet--update-projects").data('url') + '&type=projects&filterId=' + $(this).val();
		$.nette.ajax({
			url: url,
			complete: function () {
				$('.select2').select2({theme: "bootstrap"});
			}
		});
	});
	
	$(document).on('click', '.editTaskModal', function(){
		$.nette.ajax({
			url: $(this).data('href')
		});
	});
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

$(function () {
    $.nette.init();
});

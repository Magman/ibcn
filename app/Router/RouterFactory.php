<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;
		
		$adminRouter = new RouteList('Admin');
		
		$adminRouter->addRoute('admin/account/<action>/<proc>/<userid>', [
						'presenter' => 'Account',
						'action' => 'default',
		]);

		$adminRouter->addRoute('admin/<presenter>/<action>/<proc>');

		$adminRouter->addRoute('admin/<presenter>/<action>', [
						'presenter' => 'Sign',
						'action' => 'in',
		]);

		$router[] = $adminRouter;
		
		$frontRouter = new RouteList('Front');
		
		$frontRouter->addRoute('index.php', 'Order:default', $frontRouter::ONE_WAY);
										
		$frontRouter->addRoute('<presenter>[/<action>][/<eventid>][/<categoryid>]', ['presenter' => 'Order',
				'action' => 'default'
		]);
		
		$router[] = $frontRouter;
		
		return $router;
	}
}

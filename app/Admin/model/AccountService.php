<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Admin\Model;

use Nette;

class AccountService  {

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function showAccount() {
        $row = $this->database->table('users')->fetchAll();
        foreach ($row as $r) {
            $account[] = [$r->id, $r->role, $r->username, $r->password, $r->active];
        }
        return $account;
    }

    public function showRole() {
        $row = $this->database->table('role')->order('id DESC')->fetchAll();
        foreach ($row as $r) {
            $role[$r->id] = $r->role;
        }
        return $role;
    }

		public function showUser($userid) {
        $row = $this->database->table('users')->get($userid);
        return $row->username;
    }

    public function numSuper() {
        $row = $this->database->table('users')->where('role = ?', 1)->fetchAll();
        return count($row);
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Admin\Model;

use Nette;

use Nette\Utils\Strings;

class OrdersService {

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function showOrder() {
        
				$orders = [];
				
				$select = $this->database->query("SELECT orders.*, event.event FROM orders "
								. "LEFT JOIN event ON orders.eventid = event.id ORDER BY orders.id DESC");
				
				if ($select != null) {
					
					foreach ($select as $s) {
						
						$customer = [];
						
						$sel = $this->database->table('customer')->where('orderid = ?', $s->id)->fetch();
						
						if ($sel) {
                                                    
                                                        switch ($sel->paymethod) {
                                                            case 1: $paymethod = 'BT';
                                                                break;
                                                            case 2: $paymethod = 'CC';
                                                            
                                                        }
						
							$customer = ['name' => $sel->name, 'surname' => $sel->surname, 'phone' => $sel->phone, 'email' => $sel->email, 'paymethod' => $paymethod];
						
						}
						
						$orders[] = ['orderid' => $s->id, 'ordernum' => Strings::padLeft($s['ordernum'], 7, '0'), 'event' => $s->event, 'dateorder' => $s->dateorder, 'customer' => $customer];	
						
					}
					
				}
				
        return $orders;
    }

		public function getOrder($orderid) {
        
				$order = [];

				$select = $this->database->query("SELECT orders.ordernum, orders.dateorder, orders.resultcode, orders.paymentstatus, orders.payid, customer.*, event.event FROM orders "
								. "LEFT JOIN customer ON orders.id = customer.orderid "
								. "LEFT JOIN event ON orders.eventid = event.id WHERE orders.id = $orderid");
				
				$total = $this->total($orderid);

				if ($select != null) {

						foreach ($select as $s) { 

								$prices = [];

								$sel = $this->database->query("SELECT orderprice.person, orderprice.price, category.category, category.pass FROM orderprice "
																. "LEFT JOIN category ON orderprice.categoryid = category.id WHERE orderprice.orderid = $orderid");  

								if ($sel != null) {

										foreach ($sel as $se) {

												$prices[] = [
														'person' => $se['person'],
														'price' => $se['price'],
														'category' => $se['category'],
														'pass' => $se['pass']                         
												];

										}

								}
                                                                
                                                                if ($s->vat == null) {
                                                                    
                                                                    $vat = 'no';
                                                                    
                                                                } else {
                                                                    
                                                                   $vat = $s->vat; 
                                                                    
                                                                }
                                                                
                                                                switch ($s->paymethod) {
                                                                    
                                                                    case 0: $paymethod = 'None';
                                                                        break;
                                                                    case 1: $paymethod = 'Bank Transfer';
                                                                        break;
                                                                    case 2: $paymethod = 'Credit Card - MasteCard/Visa';
                                                                    
                                                                }

								$order = [
										'event' => $s->event,
										'ordernum' => Strings::padLeft($s->ordernum, 7, '0'),
										'dateorder' => $s->dateorder,
                                                                                'resultcode' => $s->resultcode,
                                                                                'paymentstatus' => $s->paymentstatus,
                                                                                'payid' => $s->payid,
										'title' => $s->title,
										'name' => $s->name,  
										'surname' => $s->surname,
										'companyname' => $s->companyname,
										'jobtitle' => $s->jobtitle,
										'email' => $s->email,
										'phone' => $s->phone,
										'address' => $s->address,
										'city' => $s->city,
                                                                                'postcode' => $s->postcode,
										'country' => $s->country,
                                                                                'vat' => $vat,
                                                                                'paymethod' => $paymethod,										
										'prices' => $prices,
										'total' => $total
								];

						}

				} 
				
				return $order;
						
    }
		
		private function total ($orderid) {
            
				$total = 0;

				$sel = $this->database->query("SELECT orderprice.person, orderprice.price, category.category, category.pass FROM orderprice "
										. "LEFT JOIN category ON orderprice.categoryid = category.id WHERE orderprice.orderid = $orderid");  

				if ($sel != null) {

						foreach($sel as $sl) {

								$total = intval($total + $sl['person'] * $sl['price']);

						}

				}

				return $total;
            
		}
			
}

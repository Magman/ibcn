<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Admin\Model;

use Nette;

class EventService {

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function showEvent() {
        
				$event = [];
				
				$select = $this->database->table('event')->order('start DESC')->fetchAll();
        
				if ($select != null) {
				
					foreach ($select as $s) {
						
							$event[] = ['id' => $s->id, 'event' => $s->event, 'info' => $s->info, 'start' => $s->start, 'days' => $s->days, 'place' => $s->place, 'active' => $s->active];
							
					}
				
				}
				
        return $event;
    }

		public function getEvent($eventid) {
        
				$event = [];
			
				$row = $this->database->table('event')->get($eventid);
        
        if ($row) {
				
						$event = ['eventid' => $eventid, 'event' => $row->event, 'info' => $row->info, 'start' => $row->start, 'days' => $row->days, 'place' => $row->place, 'info' => $row->info, 'active' => $row->active];
				
				}

        return $event;
    }
		
		public function getItem($eventid) {
        
				$item = null;
			
				$row = $this->database->table('event')->get($eventid);
        
        if ($row) {
				
						$item = $row->event;
				
				}

        return $item;
    }		
		
}

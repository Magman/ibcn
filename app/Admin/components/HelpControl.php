<?php

use Nette\Application\UI\Control;

class HelpControl extends Control {

	public function getHelp($section) {

            foreach ($section as $s) {
                $file = $s;
            }

            $help = readfile('help/help_' . $file . '.txt');

            return $help[0]; // bez poctu znaku

	}

	public function render($section) {
		$template = $this->template;
		$template->setFile(__DIR__ . '/HelpControl.latte');
		// pošleme do šablony parametry
		$template->help = $this->getHelp($section);
		// a vykreslíme ji
		$template->render();
	}

}

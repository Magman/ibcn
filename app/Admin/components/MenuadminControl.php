<?php

use Nette\Application\UI\Control;

class MenuadminControl extends Control {

	public function getMenuadmin() {
		
			return ['Account' => 'Users', 'Event' => 'Events', 'Order' => 'Orders'];
	}

	public function render() {
		$template = $this->template;
		$template->setFile(__DIR__ . '/MenuadminControl.latte');
		// pošleme do šablony parametry
		$template->menuadminArr = $this->getMenuadmin();
		// a vykreslíme ji
		$template->render();
	}

}

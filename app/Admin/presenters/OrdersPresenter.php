<?php

namespace App\Admin\Presenters;

use Nette\Utils\Strings;

use Nette\Application\UI\Form;

class OrdersPresenter extends \App\Admin\Presenters\AdminPresenter {

	/** @var \App\Admin\Model\OrdersService @inject */
	public $order;

	/** @var int */
	public $proc;

	/** @var int */
	public $orderid;
	
	/** @var array */
	public $orders;
	
	/** @var array */
	public $ordercomplete;

	public function startup() {
			parent::startup();

			$this->proc = intval($this->request->getParameter('proc'));

			$this->orderid = intval($this->request->getParameter('orderid'));
			
			$this->orders = $this->order->showOrder();
			
			$this->ordercomplete = $this->order->getOrder($this->orderid);

	}
	
	public function renderDefault() { 
		$this->template->proc = $this->proc;
		$this->template->orderid = $this->orderid;
		$this->template->orders = $this->orders;
		$this->template->ordercomplete = $this->ordercomplete;
	}

}

<?php

namespace App\Admin\Presenters;

use Nette\Application\UI\Form;
use Nette\Security\Passwords;

class AccountPresenter extends \App\Admin\Presenters\AdminPresenter {

    /** @var \App\Admin\Model\AccountService @inject */
    public $account;

    /** @var int */
    public $proc;

    /** @var int */
    public $userid;

    /** @var int */
    public $loggedRole;

    /** @var int */
    public $numSuper;

    /** @var string */
    public $name;

    public function startup() {
        parent::startup();

        $proc = intval($this->request->getParameter('proc'));

        $this->proc = $proc;

        $userid = intval($this->request->getParameter('userid'));

        $this->userid = $userid;

				if ($this->userid) {

            $this->name = $this->account->showUser($this->userid); // editovany
        }

        if ($this->user->isLoggedIn()) {

            $this->loggedRole = $this->database->table('users')->get($this->user->id)['role'];
        }

        $this->numSuper = $this->account->numSuper();
    }

    protected function createComponentAcceditForm() {
        $form = new Form;

        $form->addProtection();

        $form->addHidden('userid');

        $form->addText('username', 'User name:');

        $row = $this->database->table('users')->get($this->userid);

        $form->addSelect('role', 'Role:', $this->account->showRole());

        $form->addPassword('password', 'New password:');

        $form->addPassword('passwordVerify', 'Password again:');

        $form->setDefaults([
            'userid' => $this->userid,
            'role' => $row['role'],
            'username' => $this->name
        ]);

        $form->addSubmit('send', 'Edit')
                ->setAttribute('class', 'btn btn-success');

        $form->getElementPrototype()->class('ajax');

        $form->onSuccess[] = [$this, 'acceditFormSucceeded'];

        return $form;
    }

    public function acceditFormSucceeded($form, $values) { // AJAX
        if (!$this->isAjax()) {

            $this->flashMessage('Error');
        } else {

            if (empty($values->username)) {

                $this->flashMessage('Fill username!');
            } else if (empty($values->password)) {

                $this->flashMessage('Fill new password!');
            } else if (strlen($values->password) < 5) {

                $this->flashMessage('Password must have min 5 letters!');
            } else if (empty($values->passwordVerify)) {

                $this->flashMessage('Fill new password again!');
            } else if ($values->password !== $values->passwordVerify) {

                $this->flashMessage('Password are different!');
            } else {

                if ($this->loggedRole === 1) {

                    if ($values->role === 2 and $this->numSuper < 2 and $this->user->id == $values->userid) {

                        $this->flashMessage('Must stay min 1 superadmin.');
                    } else {

                        try {

                            $this->database->query('UPDATE users SET ? WHERE id=?', [
                                'role' => $values->role,
                                'username' => $values->username,
                                'password' => password_hash($values->password, PASSWORD_DEFAULT)
														], $values->userid);

                            $this->flashMessage('Edit successfully.');

                            $this->template->numSuper = $this->numSuper;

														$this->redrawControl('editform');

                        } catch (Exception $e) {

                            echo 'Caught exception: ', $e->getMessage(), "\n";
                        }
                    }
                } else {

                    $this->flashMessage('You can not edit this accout (superadmin).');
                }

                $this->redrawControl('tableUsers');
            }
        }

        $this->redrawControl('flashesAdmin');
    }

    protected function createComponentAccdeleteForm() {
        $form = new Form;

        $form->addProtection();

        $form->addHidden('userid');

        $form->addHidden('role');

        $row = $this->database->table('users')->get($this->userid);

        $form->setDefaults([
            'userid' => $this->userid,
            'role' => $row['role']
        ]);

        $form->addSubmit('send', 'Smazat')
                ->setAttribute('class', 'btn btn-danger');

        $form->getElementPrototype()->class('ajax');

        $form->onSuccess[] = [$this, 'accdeleteFormSucceeded'];

        return $form;
    }

    public function accdeleteFormSucceeded($form, $values) { // AJAX
        if (!$this->isAjax()) {

            $this->flashMessage('Error');
        } else {

            if ($this->loggedRole === 1) { // superadmin

                if ($this->numSuper == 1 and $this->user->id == $values->userid) {

                    $this->flashMessage('Must stay min 1 superadmin.');
                } else {

                    try {

                        $this->database->query('DELETE from users WHERE id=?', $values->userid);

                        $this->template->numSuper = $this->account->numSuper();

                        $this->flashMessage('Deleting successfully.');
                    } catch (Exception $e) {

                        echo 'Caught exception: ', $e->getMessage(), "\n";
                    }
                }
            } else {

                $this->flashMessage('You can not edit this accout (superadmin).');
            }

            $this->redrawControl('tableUsers');
        }

        $this->redrawControl('flashesAdmin');
    }

    protected function createComponentAccinsertForm() {
        $form = new Form;

        $form->addProtection();

        $form->addText('username', 'Username:')
								->setDefaultValue('username')
								->setAttribute('class', 'empty');

        $form->addSelect('role', 'Role:', $this->account->showRole());

        $form->addPassword('password', 'New password:');

        $form->addPassword('passwordVerify', 'New password again:');

        $form->addSubmit('send', 'Uložit')
                ->setAttribute('class', 'btn btn-success');

        $form->getElementPrototype()->class('ajax');

        $form->onSuccess[] = [$this, 'accinsertFormSucceeded'];

        return $form;
    }

    public function accinsertFormSucceeded($form, $values) { // AJAX
        if (!$this->isAjax()) {

            $this->flashMessage('Error');
        } else {

            if (empty($values->username)) {

                $this->flashMessage('Fill username!');
            } else if (empty($values->password)) {

                $this->flashMessage('Fill password!');
            } else if (strlen($values->password) < 5) {

                $this->flashMessage('Password must have min 5 letters!');
            } else if (empty($values->passwordVerify)) {

                $this->flashMessage('Fill new password againg!');
            } else if ($values->password !== $values->passwordVerify) {

                $this->flashMessage('Passwords are different!');
            } else {

                if ($this->loggedRole === 1) {

                    try {

												$row = $this->database->table('users')->where('username = ?', $values->username)->fetch();

												if (!$row) {

														$this->database->query('INSERT INTO users', [
																'role' => intval($values->role),
																'username' => $values->username,
																'password' => password_hash($values->password, PASSWORD_DEFAULT)
														]);

														$this->flashMessage('Adding successfully.');

														$this->redrawControl('insertform');

												} else {

														$this->flashMessage('Choose other username.');
												}

                    } catch (Exception $e) {

                        echo 'Caught exception: ', $e->getMessage(), "\n";
                    }
                } else {

                    $this->flashMessage('You can not edit this account (superadmin).');
                }

                $this->redrawControl('tableUsers');
            }
        }

        $this->redrawControl('flashesAdmin');
    }

    protected function createComponentAccactiveForm() {
        $form = new Form;

        $form->addProtection();

        $form->addHidden('userid');

        $active = [1 => ' active', 0 => ' no'];

        $row = $this->database->table('users')->get($this->userid);

        $form->addRadioList('active', $row['username']. ': ', $active);

        $form->setDefaults([
            'userid' => $this->userid,
            'active' => $row['active']
        ]);

        $form->addSubmit('send', 'Change')
                ->setAttribute('class', 'btn btn-success');

        $form->getElementPrototype()->class('ajax');

        $form->onSuccess[] = [$this, 'accactiveFormSucceeded'];

        return $form;
    }

    public function accactiveFormSucceeded($form, $values) { // AJAX
        if (!$this->isAjax()) {

            $this->flashMessage('Error');
        } else {

            if ($this->loggedRole === 1) {


                if ($values->active === 0 and $this->numSuper < 2 and $this->user->id == $values->userid) {

                    $this->flashMessage('Must stay min 1 superadmin');
                } else {


                    try {

                        $this->database->query('UPDATE users SET ? WHERE id=?', [
                            'active' => $values->active,
                                ], $values->userid);

                        $this->flashMessage('Editing successfully.');
                    } catch (Exception $e) {

                        echo 'Caught exception: ', $e->getMessage(), "\n";
                    }
                }
            } else {

                $this->flashMessage('You can not edit this account (superadmin).');
            }

            $this->redrawControl('tableUsers');
        }

        $this->redrawControl('flashesAdmin');
    }

    public function renderDefault() { // Default:default
        $this->template->account = $this->account->showAccount();
        $this->template->role = $this->account->showRole();
        $this->template->proc = $this->proc;
        $this->template->userid = $this->userid;
				$this->template->name = $this->name;

    }

}

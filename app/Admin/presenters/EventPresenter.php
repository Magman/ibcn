<?php

namespace App\Admin\Presenters;

use Nette\Utils\Strings;

use Nette\Application\UI\Form;

class EventPresenter extends \App\Admin\Presenters\AdminPresenter {

	/** @var \App\Admin\Model\EventService @inject */
	public $event;

	/** @var int */
	public $proc;

	/** @var int */
	public $eventid;
	
		/** @var array */
	public $events;

	/** @var string */
	public $item = null;

	public function startup() {
			parent::startup();

			$this->proc = intval($this->request->getParameter('proc'));

			$this->eventid = intval($this->request->getParameter('eventid'));
			
			$this->events = $this->event->showEvent();
			
			if ($this->eventid != 0) {
				
					$this->item = $this->event->getItem($this->eventid);
			}

	}
	
	private function checknumber($num) {

			if (substr($num, 0, 1) === '0') {

					$checknum = substr($num, 1, 1);
			} else {

				 $checknum = $num; 
			}

			return $checknum;

	}
	
	protected function createComponentEvinsertForm() {
			
		$form = new Form;

			$form->addProtection();
			
			$form->addText('event', 'Event:')
							->setAttribute('class','td440');

			$form->addText('start', 'Date:')
							->setAttribute('id','datepicker');
			
			$data = [
					'1' => 1,
					'2' => 2,
					'3' => 3,
					'4' => 4,
					'5' => 5,
					'6' => 6,
					'7' => 7
			];
			
			$form->addSelect('days', 'Days:', $data);

			$form['start']->setDefaultValue(date('Y-m-d'));
			
			$form->addText('place', 'Place:')
							->setAttribute('class','td240');

			$form->addSubmit('send', 'Save')
							->setAttribute('class', 'btn btn-success');

			$form->getElementPrototype()->class('ajax');

			$form->onSuccess[] = [$this, 'evinsertFormSucceeded'];

			return $form;
	}

	public function evinsertFormSucceeded($form, $values) {
			
			if (!$this->isAjax()) {

					$this->flashMessage('Error');
			} else {

					if (empty($values->event)) {

							$this->flashMessage('Fill event name!');
					} else if (empty($values->start)) {

							$this->flashMessage('Fill starting date!');
					} else {

							try {

									if (checkdate($this->checknumber(substr($values->start, 5, 2)), $this->checknumber(substr($values->start, 8, 2)), substr($values->start, 0, 4)) == true) {

											$this->database->query('START TRANSACTION');	
										
											$this->database->query('INSERT INTO event', [
													'event' => $values->event,
													'start' =>  $values->start,
													'days' => $values->days,
													'place' => $values->place,
											]);
											
											$eventid = $this->database->table('event')->max('id');

											$this->database->query('INSERT INTO price', [
													'eventid' => $eventid,
													'categoryid' =>  1,
											]);
											
											$this->database->query('INSERT INTO price', [
													'eventid' => $eventid,
													'categoryid' =>  2,
											]);
																						
											$this->database->query('INSERT INTO price', [
													'eventid' => $eventid,
													'categoryid' =>  3,
											]);
																																	
											$this->database->query('INSERT INTO price', [
													'eventid' => $eventid,
													'categoryid' =>  4,
											]);
											
											$this->database->query('INSERT INTO price', [
													'eventid' => $eventid,
													'categoryid' =>  5,
											]);
											
											$insert = $this->database->query('COMMIT');
																					
											if ($insert) {

													$this->flashMessage('Adding successfully.');

													$this->redrawControl('insertform');

													$this->events = $this->event->showEvent();

													$this->redrawControl('tableEvent');

											} else {

													$this->flashMessage('Error!');
											}

									} else {

											$this->flashMessage('Date is not valid!');
									}
							} catch (Exception $e) {

									echo 'Caught exception: ', $e->getMessage(), "\n";
							}

					}
			}

			$this->redrawControl('flashesAdmin');
	}
	
	protected function createComponentEveditForm() {
			
		$form = new Form;

			$form->addProtection();
			
			$form->addHidden('eventid', $this->eventid);
			
			$form->addText('event', 'Event:')
							->setAttribute('class','td440');

			$form->addText('start', 'Date:')
							->setAttribute('id','datepicker');
			
			$data = [
					'1' => 1,
					'2' => 2,
					'3' => 3,
					'4' => 4,
					'5' => 5,
					'6' => 6,
					'7' => 7
			];
			
			$form->addSelect('days', 'Days:', $data);
			
			$form->addText('place', 'Place:')
							->setAttribute('class','td240');
			
			$form->addTextArea('info', 'Info:')          
            ->setAttribute('class', 'ckeditor');
			
			$row = $this->database->table('event')->get($this->eventid);

			if ($row) {
			
				$form->setDefaults([
							'event' => $row['event'],
							'start' => substr($row['start'], 0, 10),
							'days' => $row['days'],
							'place' => $row['place'],
							'info' => $row['info']
				]);
				
			}
							
			$form->addSubmit('send', 'Save')
							->setAttribute('class', 'btn btn-success');
			
			$form->getElementPrototype()->onclick('CKEDITOR.instances["' . $form['info']->getHtmlId() . '"].updateElement()');

			$form->getElementPrototype()->class('ajax');

			$form->onSuccess[] = [$this, 'eveditFormSucceeded'];

			return $form;
	}

	public function eveditFormSucceeded($form, $values) {
			
			if (!$this->isAjax()) {

					$this->flashMessage('Error');
			} else {

					if (empty($values->event)) {

							$this->flashMessage('Fill event name!');
					} else if (empty($values->start)) {

							$this->flashMessage('Fill starting date!');
					} else {

							try {

									if (checkdate($this->checknumber(substr($values->start, 5, 2)), $this->checknumber(substr($values->start, 8, 2)), substr($values->start, 0, 4)) == true) {

											$data = [
													'event' => $values->event,
													'start' =>  $values->start,
													'days' => $values->days,
													'place' => $values->place,
													'info' => $values->info
											];											
										
										
											$update = $this->database->query('UPDATE event SET ? WHERE id=?', $data, $values->eventid);

											if ($update) {

													$this->flashMessage('Editing successfully.');

													$this->redrawControl('insertform');

													$this->events = $this->event->showEvent();

													$this->redrawControl('tableEvent');

											} else {

													$this->flashMessage('Error!');
											}

									} else {

											$this->flashMessage('Date is not valid!');
									}
							} catch (Exception $e) {

									echo 'Caught exception: ', $e->getMessage(), "\n";
							}

					}
			}

			$this->redrawControl('flashesAdmin');
	}
	
	protected function createComponentActiveForm() {
		
		$form = new Form;

		$form->addProtection();

		$form->addHidden('eventid');

		$active = [1 => ' active', 0 => ' no'];

		$row = $this->database->table('event')->get($this->eventid);
		
		$string = null;
		
		if ($row) {
			
			$string = Strings::truncate($row['event'], 30);
		}
		
		$form->addRadioList('active', $string . ': ', $active);

		$form->setDefaults([
				'eventid' => $this->eventid,
				'active' => $row['active']
		]);

		$form->addSubmit('send', 'Change')
						->setAttribute('class', 'btn btn-success');

		$form->getElementPrototype()->class('ajax');

		$form->onSuccess[] = [$this, 'activeFormSucceeded'];

		return $form;
	}

	public function activeFormSucceeded($form, $values) { // AJAX
		if (!$this->isAjax()) {

			$this->flashMessage('Error');
		} else {

			try {

				$this->database->query('UPDATE event SET ? WHERE id=?', [
						'active' => $values->active,
								], $values->eventid);

				$this->flashMessage('Editing successfully.');
			} catch (Exception $e) {

				echo 'Caught exception: ', $e->getMessage(), "\n";
			}
			
			$this->events = $this->event->showEvent();

			$this->redrawControl('tableEvent');
		}

		$this->redrawControl('flashesAdmin');
	}
	
	protected function createComponentDeleteForm() {
		
		$form = new Form;

		$form->addProtection();

		$form->addHidden('eventid');

		$form->setDefaults([
				'eventid' => $this->eventid
		]);

		$form->addSubmit('send', 'Delete')
						->setAttribute('class', 'btn btn-danger');

		$form->getElementPrototype()->class('ajax');

		$form->onSuccess[] = [$this, 'deleteFormSucceeded'];

		return $form;
}

public function deleteFormSucceeded($form, $values) { // AJAX
		
		if (!$this->isAjax()) {

				$this->flashMessage('Error');
		} else {

				try {

						$this->database->query('DELETE from event WHERE id=?', $values->eventid);


						$this->flashMessage('Deleting successfully.');
				
						
				} catch (Exception $e) {

						echo 'Caught exception: ', $e->getMessage(), "\n";
				}
				
				$this->events = $this->event->showEvent();

				$this->redrawControl('tableEvent');
		}

		$this->redrawControl('flashesAdmin');
}

	protected function createComponentPriceForm() {
		
		$form = new Form;

		$form->addProtection();

		$form->addHidden('eventid');
		
		$form->addText('price1');
		
		$form->addText('price2');
		
		$form->addText('price3');
		
		$form->addText('price4');
		
		$form->addText('price5');
				
		if ($this->eventid != 0) {
			
			$price = [];
			
			$select = $this->database->table('price')->where('eventid = ?', $this->eventid)->fetchAll();
			
			if ($select != null) {
				
				foreach ($select as $s) {
				
					$price[] = $s->price;
								
				}
			}
		
			$form->setDefaults([
					'price1' => $price[0],
					'price2' => $price[1],
					'price3' => $price[2],
					'price4' => $price[3],
					'price5' => $price[4],
					'eventid' => $this->eventid
			]);
		
		}

		$form->addSubmit('send', 'Send')
						->setAttribute('class', 'btn btn-danger');

		$form->getElementPrototype()->class('ajax');

		$form->onSuccess[] = [$this, 'priceFormSucceeded'];

		return $form;
}

public function priceFormSucceeded($form, $values) { // AJAX
		
		if (!$this->isAjax()) {

				$this->flashMessage('Error');
				
		} else {

				try {
							
						$this->database->query('UPDATE price SET price = ' . $values->price1 . ' WHERE eventid = ' . $values->eventid . ' AND categoryid = 1');

						$this->database->query('UPDATE price SET price = ' . $values->price2 . ' WHERE eventid = ' . $values->eventid . ' AND categoryid = 2');

						$this->database->query('UPDATE price SET price = ' . $values->price3 . ' WHERE eventid = ' . $values->eventid . ' AND categoryid = 3');

						$this->database->query('UPDATE price SET price = ' . $values->price4 . ' WHERE eventid = ' . $values->eventid . ' AND categoryid = 4');

						$this->database->query('UPDATE price SET price = ' . $values->price5 . ' WHERE eventid = ' . $values->eventid . ' AND categoryid = 5');
						
						$this->flashMessage('Editing successfully.');
				
						
				} catch (Exception $e) {

						echo 'Caught exception: ', $e->getMessage(), "\n";
				}
				
		}

		$this->redrawControl('flashesAdmin');
}

	
	public function renderDefault() { 
		$this->template->proc = $this->proc;
		$this->template->events = $this->events;
		$this->template->item = $this->item;
	}

}

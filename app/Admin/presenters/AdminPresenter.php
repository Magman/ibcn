<?php

namespace App\Admin\Presenters;

use Nette;

/**
 * Base presenter for all application presenters.
 */
abstract class AdminPresenter extends Nette\Application\UI\Presenter {

    /** @var Nette\Database\Context  */
    public $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    protected function createComponentMenuadmin() {
        $menu = new \MenuadminControl;
        return $menu;
    }

    protected function createComponentHelp($section) {
        $help = new \HelpControl;
        return $help;
    }

}

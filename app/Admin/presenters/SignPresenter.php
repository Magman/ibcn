<?php

namespace App\Admin\Presenters;

use Nette;

use Nette\Application\UI\Form;

class SignPresenter extends AdminPresenter
{

    private $passwords;

    public function __construct(Nette\Database\Context $database, Nette\Security\Passwords $passwords)
    {

				$this->passwords = $passwords;

				$this->database = $database;
    }

    protected function createComponentSignInForm()
		{
				$form = new Form;

				$form->addProtection();

				$form->addText('username', 'Uživatelské jméno:')
					->setRequired('Prosím vyplňte své uživatelské jméno.');

				$form->addPassword('password', 'Heslo:')
					->setRequired('Prosím vyplňte své heslo.');

				$form->addSubmit('send', 'Přihlásit')
														->setAttribute('class', 'btn btn-info');

				$form->onSuccess[] = [$this, 'signInFormSucceeded'];

				return $form;
		}

		public function signInFormSucceeded($form, $values)
		{

				$row = $this->database->table('users')->where('username = ?', $values->username)->fetch();

				if (!$row) {
						// throw new Nette\Security\AuthenticationException('User not found.');

						$this->flashMessage('Uživatelské jméno není správné!');

				} else if (!$this->passwords->verify($values->password, $row->password)) {

						//throw new Nette\Security\AuthenticationException('Invalid password.');

						$this->flashMessage('Heslo není správné!');

				} else {

						$this->getUser()->login(new Nette\Security\Identity($row->id, $row->role, ['username' => $row->username]));

						$this->redirect('Account:default');

				}

		}

		public function actionOut()
		{
				$this->getUser()->logout();
		}

}

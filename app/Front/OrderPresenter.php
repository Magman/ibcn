<?php

declare(strict_types=1);

namespace App\Front\Presenters;

use Nette;

use Nette\Application\UI\Form;

final class OrderPresenter extends Nette\Application\UI\Presenter
{

    /** @var \App\Front\Model\OrderService @inject */
    public $order;

    /** @var int */
    public $eventid;

    /** @var int */
    public $categoryid;
    
    /** @var int */
    public $orderid;
    
    /** @var int */
    public $proc;
		
    /** @var string */
    public $item;
		
    /** @var string */
    public $formtype;
		
    /** @var array */
    public $showprice;
    
    /** @var array */
    public $getorder;
		
    /** @var Nette\Database\Context */
    private $database;
		
    public function __construct(\Nette\Database\Context $database) {
        parent::__construct();
        $this->database = $database;
    }
		
    public function startup() {
				
        parent::startup();

        $this->eventid = intval($this->request->getParameter('eventid'));
				
        $this->categoryid = intval($this->request->getParameter('categoryid'));
        
        $this->proc = intval($this->request->getParameter('proc'));

        if ($this->eventid != null) {

                $this->item = $this->order->getItem($this->eventid);

                if ($this->order->showPrice($this->eventid)[1]['price'] === 0) {

                        $this->formtype = 3;

                        $this->showprice = [
                            $this->order->showPrice($this->eventid)[0]['price'],
                            $this->order->showPrice($this->eventid)[2]['price'],
                            $this->order->showPrice($this->eventid)[4]['price']
                        ];

                } else {

                        $this->formtype = 5;

                        $this->showprice = [
                            $this->order->showPrice($this->eventid)[0]['price'],
                            $this->order->showPrice($this->eventid)[1]['price'],
                            $this->order->showPrice($this->eventid)[2]['price'],
                            $this->order->showPrice($this->eventid)[3]['price'],
                            $this->order->showPrice($this->eventid)[4]['price']
                        ];

                }
        }

    }
		
    protected function createComponentOrderForm() {
        
        $form = new Form;

        $form->addProtection();

        $form->addHidden('eventid');
				
        $form->addHidden('formtype');

        if ($this->eventid != 0) {

                $price = $this->order->showPrice($this->eventid);

                if ($price != null) {

                        $data = [
                                        0 => '0',
                                        1 => '1',
                                        2 => '2',
                                        3 => '3',
                                        4 => '4',
                                        5 => '5',
                                        6 => '6',
                                        7 => '7',
                                        8 => '8',
                                        9 => '9',
                                        10 => '10'
                        ];

                        $formtype = 0;

                        if ($price[1]['price'] === 0) {

                                        if ($this->categoryid === $price[0]['categoryid']) {

                                                $form->addSelect('categoryid_1', $price[0]['category'] . ' / ' . $price[0]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select1');

                                        } else {

                                                $form->addSelect('categoryid_1', $price[0]['category'] . ' / ' . $price[0]['pass'], $data)
                                                                                ->setAttribute('class', 'select1');

                                        }		

                                        if ($this->categoryid === $price[2]['categoryid']) {

                                                $form->addSelect('categoryid_3', $price[2]['category'] . ' / ' . $price[2]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select3');

                                        } else {

                                                $form->addSelect('categoryid_3', $price[2]['category'] . ' / ' . $price[2]['pass'], $data)
                                                                                ->setAttribute('class', 'select3');

                                        }	

                                        if ($this->categoryid === $price[4]['categoryid']) {

                                                $form->addSelect('categoryid_5', $price[4]['category'] . '  ' . $price[4]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select5');

                                        } else {

                                                $form->addSelect('categoryid_5', $price[4]['category'] . '  ' . $price[4]['pass'], $data)
                                                                                ->setAttribute('class', 'select5');

                                        }

                                        $formtype = 3;


                        } else {

                                        if ($this->categoryid === $price[0]['categoryid']) {

                                                $form->addSelect('categoryid_1', $price[0]['category'] . ' / ' . $price[0]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select1');

                                        } else {

                                                $form->addSelect('categoryid_1', $price[0]['category'] . ' / ' . $price[0]['pass'], $data)
                                                                                ->setAttribute('class', 'select1');

                                        }	

                                        if ($this->categoryid === $price[1]['categoryid']) {

                                                $form->addSelect('categoryid_2', $price[1]['category'] . ' / ' . $price[1]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select2');

                                        } else {

                                                $form->addSelect('categoryid_2', $price[1]['category'] . ' / ' . $price[1]['pass'], $data)
                                                                                ->setAttribute('class', 'select2');

                                        }	

                                        if ($this->categoryid === $price[2]['categoryid']) {

                                                $form->addSelect('categoryid_3', $price[2]['category'] . ' / ' . $price[2]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select3');

                                        } else {

                                                $form->addSelect('categoryid_3', $price[2]['category'] . ' / ' . $price[2]['pass'], $data)
                                                                                ->setAttribute('class', 'select3');

                                        }	

                                        if ($this->categoryid === $price[3]['categoryid']) {

                                                $form->addSelect('categoryid_4', $price[3]['category'] . ' / ' . $price[3]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select4');

                                        } else {

                                                $form->addSelect('categoryid_4', $price[3]['category'] . ' / ' . $price[3]['pass'], $data)
                                                                                ->setAttribute('class', 'select4');

                                        }	

                                        if ($this->categoryid === $price[4]['categoryid']) {

                                                $form->addSelect('categoryid_5', $price[4]['category'] . '  ' . $price[4]['pass'], $data)
                                                                                ->setDefaultValue(1)
                                                                                ->setAttribute('class', 'select5');

                                        } else {

                                                $form->addSelect('categoryid_5', $price[4]['category'] . '  ' . $price[4]['pass'], $data)
                                                                                ->setAttribute('class', 'select5');

                                        }

                                        $formtype = 5;							
                        }

                        $form->setDefaults([
                                        'eventid' => $this->eventid,
                                        'categoryid' => $this->categoryid,
                                        'formtype' => $formtype,
                        ]);

                }

        }
        
        $form->addText('title', 'Title');
        
        $form->addText('name', 'Name*')
                ->setRequired('You have not fill Name!');
        
        $form->addText('surname', 'Surname*')
                ->setRequired('You have not fill Surname!');
        
        $form->addText('companyname', 'Company name*')
                ->setRequired('You have not fill Company name!');
        
        $form->addText('jobtitle', 'Job title*')
                ->setRequired('You have not fill Job title!');
        
        $form->addText('email', 'E-mail*')
                ->setRequired('You have not fill E-mail!')
                ->addRule(Form::EMAIL, 'This is not valid e-mail!');
        
        $form->addText('phone', 'Phone number*')
                ->setRequired('You have not fill Phone number!');
        
        $form->addText('address', 'Billing address*')
                ->setRequired('You have not fill Billing address!');
        
        $form->addText('city', 'City*')
                ->setRequired('You have not fill City!');
        
        $form->addText('postcode', 'Post code*')
                ->setRequired('You have not fill Post code!');
				
				$country = [
            'Afghanistan' => 'Afghanistan',
            'Albania' => 'Albania',
            'Algeria' => 'Algeria',
            'American Samoa' => 'American Samoa',
            'Andorra' => 'Andorra',
            'Angola' => 'Angola',
            'Anguilla' => 'Anguilla',
            'Antarctica' => 'Antarctica',
            'Antigua and Barbuda' => 'Antigua and Barbuda',
            'Argentina' => 'Argentina',
            'Armenia' => 'Armenia',
            'Aruba' => 'Aruba',
            'Australia' => 'Australia',
            'Austria' => 'Austria',
            'Azerbaijan' => 'Azerbaijan',
            'Bahamas' => 'Bahamas',
            'Bahrain' => 'Bahrain',
            'Bangladesh' => 'Bangladesh',
            'Barbados' => 'Barbados',
            'Belarus' => 'Belarus',
            'Belgium' => 'Belgium',
            'Belize' => 'Belize',
            'Benin' => 'Benin',
            'Bermuda' => 'Bermuda',
            'Bhutan' => 'Bhutan',
            'Bolivia' => 'Bolivia',
            'Bosnia and Herzegovina' => 'Bosnia and Herzegovina',
            'Botswana' => 'Botswana',
            'Bouvet Island' => 'Bouvet Island',
            'Brazil' => 'Brazil',
            'British Indian Ocean Territory' => 'British Indian Ocean Territory',
            'Brunei Darussalam' => 'Brunei Darussalam',
            'Bulgaria' => 'Bulgaria',
            'Burkina Faso' => 'Burkina Faso',
            'Burundi' => 'Burundi',
            'Cambodia' => 'Cambodia',
            'Cameroon' => 'Cameroon',
            'Canada' => 'Canada',
            'Cape Verde' => 'Cape Verde',
            'Cayman Islands' => 'Cayman Islands',
            'Central African Republic' => 'Central African Republic',
            'Chad' => 'Chad',
            'Chile' => 'Chile',
            'China' => 'China',
            'Christmas Island' => 'Christmas Island',
            'Cocos (Keeling) Islands' => 'Cocos (Keeling) Islands',
            'Colombia' => 'Colombia',
            'Comoros' => 'Comoros',
            'Congo' => 'Congo',
            'Congo, The Democratic Republic of The' => 'Congo, The Democratic Republic of The',
            'Cook Islands' => 'Cook Islands',
            'Costa Rica' => 'Costa Rica',
            'Cote Divoire' => 'Cote Divoire',
            'Croatia' => 'Croatia',
            'Cuba' => 'Cuba',
            'Cyprus' => 'Cyprus',
            'Czech Republic' => 'Czech Republic',
            'Denmark' => 'Denmark',
            'Djibouti' => 'Djibouti',
            'Dominica' => 'Dominica',
            'Dominican Republic' => 'Dominican Republic',
            'Ecuador' => 'Ecuador',
            'Egypt' => 'Egypt',
            'El Salvador' => 'El Salvador',
            'Equatorial Guinea' => 'Equatorial Guinea',
            'Eritrea' => 'Eritrea',
            'Estonia' => 'Estonia',
            'Ethiopia' => 'Ethiopia',
            'Falkland Islands (Malvinas)' => 'Falkland Islands (Malvinas)',
            'Faroe Islands' => 'Faroe Islands',
            'Fiji' => 'Fiji',
            'Finland' => 'Finland',
            'France' => 'France',
            'French Guiana' => 'French Guiana',
            'French Polynesia' => 'French Polynesia',
            'French Southern Territories' => 'French Southern Territories',
            'Gabon' => 'Gabon',
            'Gambia' => 'Gambia',
            'Georgia' => 'Georgia',
            'Germany' => 'Germany',
            'Ghana' => 'Ghana',
            'Gibraltar' => 'Gibraltar',
            'Greece' => 'Greece',
            'Greenland' => 'Greenland',
            'Grenada' => 'Grenada',
            'Guadeloupe' => 'Guadeloupe',
            'Guam' => 'Guam',
            'Guatemala' => 'Guatemala',
            'Guinea' => 'Guinea',
            'Guinea-bissau' => 'Guinea-bissau',
            'Guyana' => 'Guyana',
            'Haiti' => 'Haiti',
            'Heard Island and Mcdonald Islands' => 'Heard Island and Mcdonald Islands',
            'Holy See (Vatican City State)' => 'Holy See (Vatican City State)',
            'Honduras' => 'Honduras',
            'Hong Kong' => 'Hong Kong',
            'Hungary' => 'Hungary',
            'Iceland' => 'Iceland',
            'India' => 'India',
            'Indonesia' => 'Indonesia',
            'Iran, Islamic Republic of' => 'Iran, Islamic Republic of',
            'Iraq' => 'Iraq',
            'Ireland' => 'Ireland',
            'Israel' => 'Israel',
            'Italy' => 'Italy',
            'Jamaica' => 'Jamaica',
            'Japan' => 'Japan',
            'Jordan' => 'Jordan',
            'Kazakhstan' => 'Kazakhstan',
            'Kenya' => 'Kenya',
            'Kiribati' => 'Kiribati',
            'Korea, Democratic Peoples Republic of' => 'Korea, Democratic Peoples Republic of',
            'Korea, Republic of' => 'Korea, Republic of',
            'Kuwait' => 'Kuwait',
            'Kyrgyzstan' => 'Kyrgyzstan',
            'Lao Peoples Democratic Republic' => 'Lao Peoples Democratic Republic',
            'Latvia' => 'Latvia',
            'Lebanon' => 'Lebanon',
            'Lesotho' => 'Lesotho',
            'Liberia' => 'Liberia',
            'Libyan Arab Jamahiriya' => 'Libyan Arab Jamahiriya',
            'Liechtenstein' => 'Liechtenstein',
            'Lithuania' => 'Lithuania',
            'Luxembourg' => 'Luxembourg',
            'Macao' => 'Macao',
            'Macedonia, The Former Yugoslav Republic of' => 'Macedonia, The Former Yugoslav Rep. of',
            'Madagascar' => 'Madagascar',
            'Malawi' => 'Malawi',
            'Malaysia' => 'Malaysia',
            'Maldives' => 'Maldives',
            'Mali' => 'Mali',
            'Malta' => 'Malta',
            'Marshall Islands' => 'Marshall Islands',
            'Martinique' => 'Martinique',
            'Mauritania' => 'Mauritania',
            'Mauritius' => 'Mauritius',
            'Mayotte' => 'Mayotte',
            'Mexico' => 'Mexico',
            'Micronesia, Federated States of' => 'Micronesia, Federated States of',
            'Moldova, Republic of' => 'Moldova, Republic of',
            'Monaco' => 'Monaco',
            'Mongolia' => 'Mongolia',
            'Montserrat' => 'Montserrat',
            'Morocco' => 'Morocco',
            'Mozambique' => 'Mozambique',
            'Myanmar' => 'Myanmar',
            'Namibia' => 'Namibia',
            'Nauru' => 'Nauru',
            'Nepal' => 'Nepal',
            'Netherlands' => 'Netherlands',
            'Netherlands Antilles' => 'Netherlands Antilles',
            'New Caledonia' => 'Netherlands Antilles',
            'New Zealand' =>  'New Zealand',
            'Nicaragua' => 'Nicaragua',
            'Niger' => 'Niger',
            'Nigeria' => 'Nigeria',
            'Niue' => 'Niue',
            'Norfolk Island' => 'Norfolk Island',
            'Northern Mariana Islands' => 'Northern Mariana Islands',
            'Norway' => 'Norway',
            'Oman' => 'Oman',
            'Pakistan' => 'Pakistan',
            'Palau' => 'Palau',
            'Palestinian Territory, Occupied' => 'Palestinian Territory, Occupied',
            'Panama' => 'Panama',
            'Papua New Guinea' => 'Papua New Guinea',
            'Paraguay' => 'Paraguay',
            'Peru' => 'Peru',
            'Philippines' => 'Philippines', 
            'Pitcairn' => 'Pitcairn',
            'Poland' => 'Poland',
            'Portugal' => 'Portugal',
            'Puerto Rico' => 'Puerto Rico',
            'Qatar' => 'Qatar',
            'Reunion' => 'Reunion',
            'Romania' => 'Romania',
            'Russian Federation' => 'Russian Federation',
            'Rwanda' => 'Rwanda',
            'Saint Helena' => 'Saint Helena',
            'Saint Kitts and Nevis' => 'Saint Kitts and Nevis',
            'Saint Lucia' => 'Saint Lucia',
            'Saint Pierre and Miquelon' => 'Saint Pierre and Miquelon',
            'Saint Vincent and The Grenadines' => 'Saint Vincent and The Grenadines',
            'Samoa' => 'Samoa',
            'San Marino' => 'San Marino',
            'Sao Tome and Principe' => 'Sao Tome and Principe',
            'Saudi Arabia' => 'Saudi Arabia',
            'Senegal' => 'Senegal',
            'Serbia and Montenegro' => 'Serbia and Montenegro',
            'Seychelles' => 'Seychelles',
            'Sierra Leone' => 'Sierra Leone',
            'Singapore' => 'Singapore',
            'Slovakia' => 'Slovakia',
            'Slovenia' => 'Slovenia',
            'Solomon Islands' => 'Solomon Islands',
            'Somalia' => 'Somalia',
            'South Africa' =>  'South Africa',
            'South Georgia and The South Sandwich Islands' => 'South Georgia and The South Sandwich Isl.',
            'Spain' => 'Spain',
            'Sri Lanka' => 'Sri Lanka',
            'Sudan' => 'Sudan',
            'Suriname' => 'Suriname',
            'Svalbard and Jan Mayen' => 'Svalbard and Jan Mayen',
            'Swaziland' => 'Swaziland',
            'Sweden' => 'Sweden',
            'Switzerland' => 'Switzerland',
            'Syrian Arab Republic' => 'Syrian Arab Republic',
            'Taiwan, Province of China' => 'Taiwan, Province of China',
            'Tajikistan' => 'Tajikistan',
            'Tanzania, United Republic of' => 'Tanzania, United Republic of',
            'Thailand' => 'Thailand',
            'Timor-leste' => 'Timor-leste',
            'Togo' => 'Togo',
            'Tokelau' => 'Tokelau',
            'Tonga' => 'Tonga',
            'Trinidad and Tobago' => 'Trinidad and Tobago',
            'Tunisia' => 'Tunisia',
            'Turkey' => 'Turkey',
            'Turkmenistan' => 'Turkmenistan',
            'Turks and Caicos Islands' => 'Turks and Caicos Islands',
            'Tuvalu' => 'Tuvalu',
            'Uganda' => 'Uganda',
            'Ukraine' => 'Ukraine',
            'United Arab Emirates' => 'United Arab Emirates',
            'United Kingdom' => 'United Kingdom',
            'United States' => 'United States',
            'United States Minor Outlying Islands' => 'United States Minor Outlying Islands',
            'Uruguay' => 'Uruguay',
            'Uzbekistan' => 'Uzbekistan',
            'Vanuatu' => 'Vanuatu',
            'Venezuela' => 'Venezuela',
            'Viet Nam' => 'Viet Nam',
            'Virgin Islands, British' => 'Virgin Islands, British',
            'Virgin Islands, U.S.' => 'Virgin Islands, U.S.',
            'Wallis and Futuna' => 'Wallis and Futuna',
            'Western Sahara' => 'Western Sahara',
            'Yemen' => 'Yemen',
            'Zambia' => 'Zambia', 
            'Zimbabwe' => 'Zimbabwe'
        ];
        
        $form->addSelect('country', 'Country* ', $country)
                    ->setPrompt('Choose country')
                    ->setRequired('You have not select country');
        
        $form->addText('vat', 'VAT or Tax ID number');
				
        $form->addCheckbox('condition')
                    ->setRequired('Please check Terms and conditions!');
        
        $form->addCheckbox('privacy')
                    ->setRequired('Please check Privacy Policy!');
        
        $data = [
            1 => 'Bank Transfer',
            2 => 'Credit Card'
        ];
        
        $form->addRadioList('paymethod', 'Payment method', $data)
                ->setDefaultValue(2)
                ->setAttribute('class', 'radio');

        $form->addSubmit('send', 'Send')
                ->setAttribute('class', 'btn btn-success');

        $form->onSuccess[] = [$this, 'orderFormSucceeded'];

        return $form;
    }

    public function orderFormSucceeded($form, $values) {

        try {

                $this->proc = 1;

                $this->database->query('START TRANSACTION');	

                $this->database->query('INSERT INTO orders', [
                                'eventid' => $values->eventid,
                ]);

                $id = $this->database->table('orders')->max('id');

                $update = $this->database->query('UPDATE orders SET ? WHERE id=?', ['ordernum' => $id], $id);

                $this->database->query('INSERT INTO customer', [
                                'orderid' => $id,
                                'title' => $values->title,
                                'name' => $values->name,
                                'surname' => $values->surname,
                                'companyname' => $values->companyname,
                                'jobtitle' => $values->jobtitle,
                                'email' => $values->email,
                                'phone' => $values->phone,
                                'address' => $values->address,
                                'city' => $values->city,
                                'postcode' => $values->postcode,
                                'country' => $values->country,
                                'vat' => $values->vat,
                                'paymethod' => $values->paymethod,
                ]);			

                if ($values->formtype == 3) {

                                if ($_POST['categoryid_1'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 1)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 1,
                                                        'person' => $_POST['categoryid_1'],
                                                        'price' => $row->price
                                        ]);

                                }

                                if ($_POST['categoryid_3'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 3)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 3,
                                                        'person' => $_POST['categoryid_3'],
                                                        'price' => $row->price
                                        ]);

                                }

                                if ($_POST['categoryid_5'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 5)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 5,
                                                        'person' => $_POST['categoryid_5'],
                                                        'price' => $row->price
                                        ]);

                                }

                }

                if ($values->formtype == 5) {

                                if ($_POST['categoryid_1'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 1)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 1,
                                                        'person' => $_POST['categoryid_1'],
                                                        'price' => $row->price
                                        ]);

                                }

                                if ($_POST['categoryid_2'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 2)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 2,
                                                        'person' => $_POST['categoryid_2'],
                                                        'price' => $row->price
                                        ]);

                                }								

                                if ($_POST['categoryid_3'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 3)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 3,
                                                        'person' => $_POST['categoryid_3'],
                                                        'price' => $row->price
                                        ]);

                                }

                                if ($_POST['categoryid_4'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 4)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 4,
                                                        'person' => $_POST['categoryid_4'],
                                                        'price' => $row->price
                                        ]);

                                }								

                                if ($_POST['categoryid_5'] != 0) {

                                        $row = $this->database->table('price')->where('eventid = ?', $values->eventid)->where('categoryid = ?', 5)->fetch();

                                        $this->database->query('INSERT INTO orderprice', [
                                                        'orderid' => $id,
                                                        'categoryid' => 5,
                                                        'person' => $_POST['categoryid_5'],
                                                        'price' => $row->price
                                        ]);

                                }

                }						

                $this->database->query('COMMIT');

                if ($values->paymethod == 2) {
                
                    $this->redirectUrl('http://easyweb4u.cz/paymentgateway/?orderid=' . $id);
                
                } elseif ($values->paymethod == 1) {
                    
                    $this->redirect('Transfer:default', ['orderid' => $id]);
                    
                }

        } catch (Exception $e) {

                        echo 'Caught exception: ', $e->getMessage(), "\n";
        }

        $this->redrawControl('flashesAdmin');
				
    }
		
    public function renderDefault() {

                    $this->template->eventid = $this->eventid;

                    $this->template->categoryid = $this->categoryid;

                    $this->template->orderid = $this->orderid;

                    $this->template->proc = $this->proc;

                    $this->template->item = $this->item;

                    $this->template->formtype = $this->formtype;

                    $this->template->showprice = $this->showprice;

                    $this->template->getorder = $this->getorder;

    }		
	
}

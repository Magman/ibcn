<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Front\Model;

use Nette;

use Nette\Utils\DateTime;

class OrderService {

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(\Nette\Database\Context $database) {
        $this->database = $database;
    }

    public function showPrice($eventid) {
			
        $price = [];

        $select = $this->database->table('price')->where('eventid = ?', $eventid)->fetchAll();
				
				if ($select != null) {
				
					foreach ($select as $s) {
						
							$row = $this->database->table('category')->get($s->categoryid);

							$price[] = ['categoryid' => $s->categoryid, 'price' => $s->price, 'category' => $row['category'], 'pass' => $row['pass']];					
					}
				
				}
					
        return $price;
    }
		
    public function getItem($eventid) {
			
        $item = [];

        $row = $this->database->table('event')->get($eventid);
				
        $end = DateTime::from($row->start);

        $end->modify('+' . $row->days - 1 . '  days');

        $item = ['event' => $row->event, 'start' => $row->start, 'end' => $end, 'place' => $row->place, 'info' => $row->info]; 
					
        return $item;
    }
    
    public function getOrder ($orderid) {
        
        $order = [];
        
        $select = $this->database->query("SELECT orders.ordernum, orders.eventid, orders.dateorder, customer.* FROM orders "
                . "LEFT JOIN customer ON orders.id = customer.orderid WHERE orders.id = $orderid");
        
        if ($select != null) {
            
            foreach ($select as $s) {
                
                $prices = [];
                
                $total = 0;
                
                $vat = null;
                
                $sel = $this->database->query("SELECT orderprice.person, orderprice.price, category.category, category.pass FROM orderprice "
                                . "LEFT JOIN category ON orderprice.categoryid = category.id WHERE orderprice.orderid = $orderid");  
                
                if ($sel != null) {
                    
                    foreach ($sel as $se) {
                        
                        $total = intval($total + $se['person'] * $se['price']);
                        
                        $prices[] = [
                            'person' => $se['person'],
                            'price' => $se['price'],
                            'category' => $se['category'],
                            'pass' => $se['pass']                         
                        ];
                        
                    }
                    
                }
                
                if ($s->vat == null) {
                    
                    $vat = 'no';
                    
                } else {
                    
                    $vat = $s->vat;
                    
                }
                
                switch ($s->paymethod) {
                    
                    case 1: $paymethod = 'Bank Transfer';
                    break;
                    case 2: $paymethod = 'Credit Card - MasteCard/Visa';
                    
                }

                $order = [
                    'ordernum' => $s->ordernum,
                    'dateorder' => $s->dateorder,
                    'title' => $s->title,
                    'name' => $s->name,  
                    'surname' => $s->surname,
                    'companyname' => $s->companyname,
                    'jobtitle' => $s->jobtitle,
                    'email' => $s->email,
                    'phone' => $s->phone,
                    'address' => $s->address,
                    'city' => $s->city,
                    'postcode' => $s->postcode,
                    'country' => $s->country,
                    'vat' => $vat,
                    'paymethod' => $paymethod,
                    'prices' => $prices,
                    'total' => $total
                ];
                
            }
            
        }
        
        return $order;
        
    }
    
    public function getEvent($orderid) {
        
        $event = [];
        
        $select = $this->database->query("SELECT orders.eventid, event.event, event.start, event.days, event.place FROM orders "
                . "LEFT JOIN event ON orders.eventid = event.id WHERE orders.id = $orderid");
        
        if ($select != null) {
            
            foreach ($select as $s) {
                
                $end = DateTime::from($s->start);

                $end->modify('+' . $s->days - 1 . '  days');
                
                $event = ['event' => $s->event, 'start' => $s->start, 'end' => $end, 'place' => $s->place];
                
            }
            
        }
        
        return $event;
        
    }
   
}



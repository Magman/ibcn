<?php

declare(strict_types=1);

namespace App\Front\Presenters;

use Nette;

use Nette\Application\UI\Form;

final class TransferPresenter extends Nette\Application\UI\Presenter
{

    /** @var \App\Front\Model\OrderService @inject */
    public $order;
    
    /** @var int */
    public $orderid;
    
    /** @var array */
    public $getorder;
    
    /** @var array */
    public $event;
		
		
    /** @var Nette\Database\Context */
    private $database;
		
    public function __construct(\Nette\Database\Context $database) {
        parent::__construct();
        $this->database = $database;
    }
		
    public function startup() {
				
        parent::startup();

        $this->orderid = intval($this->request->getParameter('orderid'));
        
        $this->getorder = $this->order->getOrder($this->orderid);
        
        $this->event = $this->order->getEvent($this->orderid);
				
    }
		
    public function renderDefault() {

            $this->template->getorder = $this->getorder;
            
            $this->template->event = $this->event;

    }		
	
}

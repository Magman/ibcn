-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pon 24. úno 2020, 20:43
-- Verze serveru: 10.4.11-MariaDB
-- Verze PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `ibcn`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `category`
--

INSERT INTO `category` (`id`, `category`, `pass`) VALUES
(1, 'End User', 'Standard Pass'),
(2, 'End User', 'Early Bird Pass'),
(3, 'Solution provider/Consultant', 'Standard Pass'),
(4, 'Solution provider/Consultant', 'Early Bird Pass'),
(5, '', 'Documentation only');

-- --------------------------------------------------------

--
-- Struktura tabulky `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `companyname` varchar(100) NOT NULL,
  `jobtitle` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `customer`
--

INSERT INTO `customer` (`id`, `orderid`, `title`, `name`, `surname`, `companyname`, `jobtitle`, `email`, `phone`, `address`, `city`, `country`) VALUES
(7, 11, 'Ing', 'Milan', 'Makovec', 'EW4U', 'programmer', 'magman@volny.cz', '603823644', 'Vokovická 48', 'Praha', 'ČR'),
(8, 12, 'Ing', 'Milan', 'Makovec', 'EW4U', 'programmer', 'magman@volny.cz', '603823644', 'Vokovická 48', 'Praha', 'ČR');

-- --------------------------------------------------------

--
-- Struktura tabulky `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `event` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `start` date NOT NULL,
  `days` tinyint(2) NOT NULL DEFAULT 1,
  `place` varchar(255) NOT NULL,
  `active` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `event`
--

INSERT INTO `event` (`id`, `event`, `info`, `start`, `days`, `place`, `active`) VALUES
(1, 'Intelligent Offshore Wind O&M Forum 2020', '<p>Intelligent Offshore Wind O&amp;M Forum 2020 is a strategic conference for senior representatives. The forum provides a common stage for industry&rsquo;s experts from all major planks to share their ideas and experiences regarding the operation and maintenance of the offshore wind industry. Experts from leading offshore wind farm operators, turbine manufacturers, asset management companies and all the stakeholders will be coming together to discuss the current challenges and future road map of offshore wind O&amp;M.&nbsp;Join us in Hamburg to examine and share best practices in operations and maintenance of offshore wind farms. You will discover the latest innovations in technology to advance growth and development of the offshore wind power industry.</p>\n', '2020-04-01', 2, 'Hamburg, Germany', 1),
(9, 'Offshore Wind Supply Chain Forum 2020', '<p>Offshore Wind Supply Chain Forum 2020 will bring an intensive interactive platform for project developers, windfarm owners-operators, contractors, turbine manufacturers, vessel operators, shipping companies, offshore foundation and floating technology experts, and so on to share their ideas in order to improve the reliability, quality and safety of ports&rsquo; logistics and wind power operations. The forum will also discuss on the latest technological developments and challenges that the supply chain is facing and will also focus on their commercial aspects.</p>\n', '2020-06-03', 2, 'Bremen, Germany', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `orderprice`
--

CREATE TABLE `orderprice` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `person` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `orderprice`
--

INSERT INTO `orderprice` (`id`, `orderid`, `categoryid`, `person`) VALUES
(1, 11, 1, 1),
(2, 11, 3, 1),
(3, 12, 2, 1),
(4, 12, 3, 2),
(5, 12, 5, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `ordernum` int(7) UNSIGNED ZEROFILL NOT NULL,
  `eventid` int(11) NOT NULL,
  `dateorder` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `orders`
--

INSERT INTO `orders` (`id`, `ordernum`, `eventid`, `dateorder`) VALUES
(11, 0000011, 1, '2020-02-24 19:40:01'),
(12, 0000012, 9, '2020-02-24 19:42:28');

-- --------------------------------------------------------

--
-- Struktura tabulky `price`
--

CREATE TABLE `price` (
  `id` int(11) NOT NULL,
  `eventid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `price`
--

INSERT INTO `price` (`id`, `eventid`, `categoryid`, `price`) VALUES
(1, 1, 1, 1699),
(2, 1, 2, 0),
(3, 1, 3, 1999),
(4, 1, 4, 0),
(5, 1, 5, 499),
(10, 9, 1, 1699),
(11, 9, 2, 1499),
(12, 9, 3, 1999),
(13, 9, 4, 1799),
(14, 9, 5, 499);

-- --------------------------------------------------------

--
-- Struktura tabulky `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'superadmin'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role` tinyint(2) NOT NULL,
  `active` tinyint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`, `active`) VALUES
(1, 'admin@easyweb4u.cz', '$2y$10$xijV/E/lpjR8p5IcQMpyN.hPWKj4.52TiDJYWNfbTAbztwguDb63u', '', 1, 1),
(8, 'Test', '$2y$10$5nJZfLovspWZZWq9fARYBO9dfR1XkFedYjihTww5s9O.DMzQzikGq', '', 2, 1);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderid` (`orderid`);

--
-- Klíče pro tabulku `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active` (`active`);

--
-- Klíče pro tabulku `orderprice`
--
ALTER TABLE `orderprice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderid` (`orderid`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Klíče pro tabulku `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ordernum` (`ordernum`),
  ADD KEY `eventid` (`eventid`);

--
-- Klíče pro tabulku `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventid` (`eventid`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Klíče pro tabulku `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pro tabulku `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pro tabulku `orderprice`
--
ALTER TABLE `orderprice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pro tabulku `price`
--
ALTER TABLE `price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pro tabulku `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Omezení pro tabulku `orderprice`
--
ALTER TABLE `orderprice`
  ADD CONSTRAINT `orderprice_ibfk_1` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Omezení pro tabulku `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `price_ibfk_1` FOREIGN KEY (`eventid`) REFERENCES `event` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
